import pandas as pd
import json

# specify file paths
xlsx_input_path = "./mitre_codes/enterprise-attack-v16.1.xlsx"
json_output_path = "./mitre_codes/mitre_code_techniques.json"
# specify used columns
col_names_techniques = [
    "tactics",
    "ID",
    "name",
    "description",
    "is sub-technique",
    "sub-technique of",
]

col_names_tactics = ["ID", "name"]

# specify sheet name
tactics_sheet_name = "tactics"

# read xlsx file
mitre_df = pd.read_excel(
    xlsx_input_path, engine="openpyxl", usecols=col_names_techniques
)
mitre_tactics_df = pd.read_excel(
    xlsx_input_path,
    sheet_name=tactics_sheet_name,
    engine="openpyxl",
    usecols=col_names_tactics,
)
tactic_name_to_id_dict = dict(zip(mitre_tactics_df["name"], mitre_tactics_df["ID"]))

# transform data to json structure
json_obj = []
for idx, row in mitre_df.iterrows():
    data = {}
    data["pk"] = row["ID"]
    data["model"] = "ucl.MitreCodeTechnique"
    fields = {}
    for col in col_names_techniques:
        if col in ["tactics", "name", "description"]:
            fields[col] = row[col]
    data["fields"] = fields
    tactic_names = data["fields"]["tactics"].split(",")
    tactic_ids = []
    for tactic_name in tactic_names:
        if tactic_name.strip() in tactic_name_to_id_dict:
            tactic_ids.append(tactic_name_to_id_dict[tactic_name.strip()])
        else:
            print("Tactic name not found: " + tactic_name)
            exit()
    data["fields"]["tactics"] = tactic_ids
    if row["is sub-technique"] is True:
        data["fields"]["sub_technique_of"] = row["sub-technique of"]
        data["fields"]["assignable"] = True
    else:
        data["fields"]["sub_technique_of"] = None
        data["fields"]["assignable"] = (
            bool(mitre_df["ID"].str.contains(row["ID"] + ".").any()) is False
        )

    json_obj.append(data)

# save/write json file
with open(json_output_path, "w") as outfile:
    outfile.write(json.dumps(json_obj, indent=4))

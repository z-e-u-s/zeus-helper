import pandas as pd
import json

# specify file paths
xlsx_input_path = "./mitre_codes/enterprise-attack-v16.1.xlsx"
json_output_path = "./mitre_codes/mitre_code_tactics.json"
# specify used columns
col_names = ["ID", "name", "description"]
# specify sheet name
sheet_name = "tactics"

# read xlsx file
mitre_tactics_df = pd.read_excel(
    xlsx_input_path, sheet_name=sheet_name, engine="openpyxl", usecols=col_names
)

# transform data to json structure
json_obj = []
for idx, row in mitre_tactics_df.iterrows():
    data = {}
    data["pk"] = row["ID"]
    data["model"] = "ucl.MitreCodeTactic"
    fields = {}
    for col in col_names:
        if col != "ID":
            fields[col] = row[col]
    data["fields"] = fields
    json_obj.append(data)

# save/write json file
with open(json_output_path, "w") as outfile:
    outfile.write(json.dumps(json_obj, indent=4))

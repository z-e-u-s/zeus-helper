import pandas as pd
import json
import os

### files taken from https://github.com/logstash-plugins/logstash-patterns-core/tree/main/patterns/ecs-v1

# specify file paths
grok_input_folder = "./grok_patterns/ecs_patterns"
json_pattern_category_output_path = "./grok_patterns/grok_pattern_categories.json"
json_pattern_output_path = "./grok_patterns/grok_patterns.json"
category_type = "ecs"

# Output Object
grok_patterns = []
grok_pattern_categories = []

if __name__ == "__main__":

    # Get all input filenames in the directory
    for filename in os.listdir(grok_input_folder):
        filepath = os.path.join(grok_input_folder, filename)
        print(f"processing... {filename}")

        with open(filepath) as f:
            lines = f.readlines()
  
        grok_pattern_category = {"id": filename, "type": category_type}
        grok_pattern_category_obj = {"model": "fda.GrokPatternCategory", "fields": grok_pattern_category}
        grok_pattern_categories.append(grok_pattern_category_obj)

        for line in lines:
            comment_bool = line.startswith('#')
            empty_bool = line.isspace() #line.startswith(' ')

            # Check if line is a comment or empty
            if not comment_bool and not empty_bool:
                id_pattern = line.split(" ")
                
                grok_pattern = dict()
                grok_pattern["id"] = id_pattern[0]
                grok_pattern["type"] = category_type

                # IF CUSTOM CATEGORY ADAPT HERE
                grok_pattern["category"] = filename
                pattern_string = ""

                # Everything except first entry is part of the pattern
                pattern_string = " ".join(id_pattern[1:])
                # Remove \n from string
                pattern_string = pattern_string.strip()
                grok_pattern["pattern"] = pattern_string

                grok_pattern_obj = {"model": "fda.GrokPattern", "fields": grok_pattern}
                grok_patterns.append(grok_pattern_obj)

    # save/write json file
    with open(json_pattern_category_output_path, "w") as outfile:
        outfile.write(json.dumps(grok_pattern_categories, indent=4))

    with open(json_pattern_output_path, "w") as outfile:
        outfile.write(json.dumps(grok_patterns, indent=4))
# Z.E.U.S. Scripts

## Requirements
Create virtual env and install needed libs. Run the following command in your terminal in the root folder of this project.

### follow these steps for bash:

```bash
python3 -m venv .venv # maybe you need to use "python" instead of "python3"
source ./.venv/bin/activate
pip install -r requirements_dev.txt
```

### or for powershell:

```powershell
python3 -m venv .venv # maybe you need to use "python" instead of "python3"
./.venv/bin/Activate.ps1
pip install -r requirements_dev.txt
```

### or for cmd on windows:

```cmd
python3 -m venv .venv # maybe you need to use "python" instead of "python3"
.\.venv\Scripts\activate
pip install -r requirements_dev.txt
```

## Scripts

### Parse mitre codes xlsx to UCL json format

The script `mitre_codes/mitre_code_techniques_parsing.py` and `mitre_codes/mitre_code_tactics_parsing.py` are parsing the file `enterprise-attack-v16.1.xlsx` to json files which includes parts of the content. The created json files (for MITRE Code techniques and tactics) are needed in the ucl-backend to import the mitre codes for later use.  
New MITRE ATT&CK xlsx-file can be found here https://attack.mitre.org/resources/working-with-attack/ under "ATT&CK in Excel".  
If you want a new version download it, add it in this folder and change the variable `xlsx_input_path` to the new file name in `mitre_codes/mitre_code_techniques_parsing.py` and `mitre_codes/mitre_code_tactics_parsing.py`.

#### Run script

Run the following command in your terminal in the root folder of this project.

```cmd
.\mitre_codes\mitre_code_techniques_parsing.py
.\mitre_codes\mitre_code_tactics_parsing.py
```
